import UIKit

struct FileItem {
    var url: URL
    var name: String {
        get { url.lastPathComponent }
    }
    
    var isDirectory: Bool
}

enum FolderContentDisplayMode {
    case filesOnly
    case foldersOnly
}

class FolderContentSelectionViewController: UITableViewController {
    var displayMode: FolderContentDisplayMode = .filesOnly
    var directoryToShow: URL?
    var completion: ((FileItem) -> Void)?
    
    var dataSource: [FileItem] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDataSource()
    }
    
    private func loadDataSource() {
        guard let directoryToShow = directoryToShow else {
            return
        }
        
        var properties: [URLResourceKey]?
        
        if displayMode == .foldersOnly {
            properties = [.isDirectoryKey]
        }
        
        do {
            let folderContent =
                try FileManager.default.contentsOfDirectory(
                    at: directoryToShow,
                    includingPropertiesForKeys: properties,
                    options: [.skipsHiddenFiles]
                )
            dataSource = folderContent.map {
                FileItem(
                    url: $0,
                    isDirectory: (displayMode == .foldersOnly)
                )
            }.sorted { $0.name < $1.name }
        } catch {
        }
    }
    
    private func closeView() {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int {
        return dataSource.count
    }
    
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "Cell",
            for: indexPath
        )
        
        cell.textLabel?.text = dataSource[indexPath.row].name
        
        return cell
    }
    
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {
        let item = dataSource[indexPath.row]
        
        completion?(item)
        
        closeView()
    }
    
}
